from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import unicodedata

driver = webdriver.Chrome()

def get_goals_for_url(url):
    driver.get(url)
    content = driver.find_elements_by_class_name('standard_tabelle')[1]
    return content.text

def get_and_store_fixture(fixture_and_url):
    (fixture, url) = fixture_and_url
    goals = get_goals_for_url(url)
    filename = "{}-{}.txt".format(fixture[0], fixture[1])
    file = open(filename, 'w')
    file.write(unicodedata.normalize('NFKD', goals).encode('ascii', 'ignore'))
    file.write('\n')
    file.close()

#team_names = ["west-ham-united",
#              "burnley-fc"]
#              "sunderland-afc",
#              "chelsea-fc"]

team_names =[
    "afc-bournemouth",
    "arsenal-fc",
    "burnley-fc",
    "chelsea-fc",
    "crystal-palace",
    "everton-fc",
    "hull-city",
    "leicester-city",
    "liverpool-fc",
    "manchester-city",
    "manchester-united",
    "middlesbrough-fc",
    "southampton-fc",
    "stoke-city",
    "sunderland-afc",
    "swansea-city",
    "tottenham-hotspur",
    "watford-fc",
    "west-bromwich-albion",
    "west-ham-united"]


fixtures = [ (home, away)
             for home in team_names
             for away in team_names
             if home != away
           ]

game_url_base = "http://www.worldfootball.net/report/premier-league-2016-2017-"

def game_url(home, away):
    return "{}{}-{}".format(game_url_base, home, away)
    
#game_urls = ["http://www.worldfootball.net/report/premier-league-2016-2017-burnley-fc-west-ham-united/",
#             "http://www.worldfootball.net/report/premier-league-2016-2017-chelsea-fc-sunderland-afc/",
#             "http://www.worldfootball.net/report/premier-league-2016-2017-sunderland-afc-chelsea-fc/"]


print(fixtures)

fixture_game_urls = [(fixture, game_url(fixture[0], fixture[1])) for fixture in fixtures]


for entry in fixture_game_urls:
    get_and_store_fixture(entry)

driver.close()

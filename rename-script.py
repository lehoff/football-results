team_names =[
    "afc-bournemouth",
    "arsenal-fc",
    "burnley-fc",
    "chelsea-fc",
    "crystal-palace",
    "everton-fc",
    "hull-city",
    "leicester-city",
    "liverpool-fc",
    "manchester-city",
    "manchester-united",
    "middlesbrough-fc",
    "southampton-fc",
    "stoke-city",
    "sunderland-afc",
    "swansea-city",
    "tottenham-hotspur",
    "watford-fc",
    "west-bromwich-albion",
    "west-ham-united"]

file = open("rename-matches.sh", 'w')

file.write("#!/bin/bash\n")

for team in team_names:
    str = "rename 's/{}-/{}+/' {}-*.txt\n".format(team, team, team)
    file.write(str)

file.close()
